<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>singularity</title>
</head>
<body>
@foreach($books as $book)
    <div>
        @if($book->image)
            <img src="{{asset('storage/'.$book->image)}}" alt="">
        @else
            <span>Without image</span>
        @endif
        <h1>{{$book->name}}</h1>
        <span>Category: @if($book->category)
                {{$book->category->name}}
            @else
                empty
            @endif</span>
        <p>Description: @if($book->description)
                {{$book->description}}
            @else
                empty
            @endif</p>
    </div>
    <hr>
@endforeach
</body>
</html>
