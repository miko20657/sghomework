<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect(\route('book.index'));
});


Route::get('/books', [\App\Http\Controllers\Books\IndexController::class, 'index'])->name('book.index');
Route::get('/categories', [\App\Http\Controllers\Categories\IndexController::class, 'index'])->name('category.index');
